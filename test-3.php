<?php 

require __DIR__ . '/vendor/autoload.php';

use FCL\Test3\PessoaFisica;
use FCL\Test3\PessoaJuridica;
use FCL\Test3\Treinador;
use FCL\Test3\Inscricao;
use FCL\Test3\Evento;

$evento = new Evento('Corrida de São Silvestre', new \DateTime('2015-12-31 08:00:00'));

$pessoaFisica = new PessoaFisica('Ludwig van Beethoven', 'contato@beethoven.com', '12345678989');
$pessoaJuridica = new PessoaJuridica('Fundação Casper Libero', 'contato@fcl.com.br', '61277273000172');
$treinador = new PessoaFisica('Treinador de Corredores', 'contato@treinador.com', '654654798798');

$inscricaoPessoaFisica = new Inscricao($pessoaFisica, $evento);
echo $inscricaoPessoaFisica->inscrever();

$inscricaoPessoaJuridica = new Inscricao($pessoaJuridica, $evento);
echo $inscricaoPessoaJuridica->inscrever();

$inscricaoTreinador = new Inscricao($treinador, $evento);
echo $inscricaoTreinador->inscrever();
