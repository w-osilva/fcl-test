<?php

require __DIR__ . '/vendor/autoload.php';

use FCL\Test1\Noticia;
use FCL\Test1\NoticiaEspecial;


$noticiaUm = new Noticia('Esta é uma noticia', new \DateTime());
$noticiaUm->cadastrar();
echo PHP_EOL;

$noticiaDois = new Noticia('Esta é outra noticia', new \DateTime());
$noticiaDois->cadastrar();
echo PHP_EOL;

$noticiaTres = new Noticia('Esta é outra noticia', new \DateTime());
$noticiaTres->cadastrar();
echo PHP_EOL;

$alterarNoticia = new Noticia("Esta notícia foi alterada", new \DateTime());
$alterarNoticia->alterar(1);
echo PHP_EOL;

$noticia = new Noticia(null, null);
$noticias = $noticia->listar();
echo PHP_EOL;

$ne1 = new NoticiaEspecial('Esta é uma noticia Especial', new \DateTime(), array('imagem1.jpg', 'imagem2.jpg'));
$ne1->cadastrar();
echo PHP_EOL;

$ne2 = new NoticiaEspecial('Esta é outra noticia Especial', new \DateTime(), array('imagem2.jpg', 'imagem4.jpg'));
$ne2->cadastrar();
