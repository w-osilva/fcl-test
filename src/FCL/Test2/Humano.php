<?php 

namespace FCL\Test2;

class Humano implements TrabalhadorInterface
{
    public function trabalhar()
    {
        return __CLASS__ . " trabalhando" . PHP_EOL; 
    }   
    
    public function descansar()
    {
        return __CLASS__ . " descansando" . PHP_EOL; 
    } 
    
    public function dormir()
    {
        return __CLASS__ . " dormindo" . PHP_EOL; 
    } 

}

