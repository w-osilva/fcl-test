<?php

namespace FCL\Test2;

interface TrabalhadorInterface
{
    /**
     * Um trabalhador deve trabalhar
     * @return string
     */
    function trabalhar();

    /**
     * Um trabalhador deve descansar
     * @return string
     */    
    function descansar();
    
    /**
     * Um trabalhador deve dormir
     * @return string
     */
    function dormir();

}
