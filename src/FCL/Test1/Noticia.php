<?php

namespace FCL\Test1;

use FCL\Test1\Database;
use FCL\Test1\Config;

class Noticia {
  protected $texto;
  protected $data;
  
  public function __construct($texto, $data) {
    $this->texto = $texto;
    $this->data = $data;
  }
  
  public function listar() {
    echo '$db = new Database(Config::$host, Config::$user, Config::$password , Config::$dbname)' . PHP_EOL;
    echo '$stmt = $db->query(\'SELECT * FROM noticias\')' . PHP_EOL;
    echo '$stmt->fetchAll(PDO::FETCH_ASSOC) // Retorna as noticias posteriormente' . PHP_EOL;
  }

  public function cadastrar() {
    echo '$db = new Database(Config::$host, Config::$user, Config::$password , Config::$dbname)' . PHP_EOL;
    echo 'Insere uma noticia no banco de dados'. PHP_EOL;
  }
  
  public function alterar($id) {
    echo '$db = new Database(Config::$host, Config::$user, Config::$password , Config::$dbname)' . PHP_EOL;
    echo 'Efetua a alteração de uma notícia pelo seu ID' . PHP_EOL;
  }
  
}
