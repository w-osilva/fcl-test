<?php 

namespace FCL\Test1;

class Database {

  protected $host;
  protected $user;
  protected $password;
  protected $dbname;
  
  public function __construct($host, $user, $password, $dbname) {
    $this->host = $host;
    $this->user = $user;
    $this->password = $password;
    $this->dbname = $dbname;
    
    echo "Uma pseudo-instância de PDO foi criada para MySQL" . PHP_EOL;
  }
  
  public function getHost(){
    return $this->host;
  }

  public function setHost($host){
    $this->host = $host;
  }

  public function getUser(){
    return $this->user;
  }

  public function setUser($user){
    $this->user = $user;
  }

  public function getPassword(){
    return $this->password;
  }

  public function setPassword($password){
    $this->password = $password;
  }

  public function getDbname(){
    return $this->dbname;
  }

  public function setDbname($dbname){
    $this->dbname = $dbname;
  }

}
