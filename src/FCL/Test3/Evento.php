<?php

namespace FCL\Test3;

class Evento
{
    protected $nome;
    protected $data;
    
    public function __construct($nome, $data)
    {
        $this->nome = $nome;
        $this->data = $data;
                
        return $this;
    }
    
}
