<?php

namespace FCL\Test3;

class PessoaFisica
{
    protected $nome;
    protected $email;
    protected $cpf;
    
    public function __construct($nome, $email, $cpf)
    {
        $this->nome = $nome;
        $this->email = $email;
        $this->cpf = $cpf;
        
        return $this;
    }
    
    public function estaInscrito()
    {
        return 'Retorna se a Pessoa Fisica está inscrito ou não para a prova' . PHP_EOL;  
    }
}
