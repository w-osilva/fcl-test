<?php

namespace FCL\Test3;

class Treinador
{
    protected $nome;
    protected $email;
    protected $cpf;
    protected $cref;
    
    public function __construct($nome, $email, $cpf, $cref)
    {
        $this->nome = $nome;
        $this->email = $email;
        $this->cpf = $cpf;
        $this->cref = $cref;
                
        return $this;
    }
    
    public function estaInscrito()
    {
        return 'Retorna se este o Treinador está inscrito ou não para a prova' . PHP_EOL;  
    }
}
