<?php

namespace FCL\Test3;

class PessoaJuridica
{
    protected $razaoSocial;
    protected $email;
    protected $cnpj;
    
    public function __construct($razaoSocial, $email, $cnpj)
    {
        $this->razaoSocial = $razaoSocial;
        $this->email = $email;
        $this->cnpj = $cnpj;
        
        return $this;
    }
    
    public function estaInscrito()
    {
        return 'Retorna se a Pessoa Juridica está inscrito ou não para a prova' . PHP_EOL;  
    }
        
}
