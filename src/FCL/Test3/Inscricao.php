<?php

namespace FCL\Test3;

class Inscricao
{
    protected $participante;
    protected $evento;
    
    public function __construct($participante, $evento)
    {
        $this->participante = $participante;
        $this->evento = $evento;
                
        return $this;
    }
       
    public function inscrever()
    {
        if ($this->participante instanceof PessoaFisica) {
            return $this->cadastrarPessoaFisica();
        } else if ($this->participante instanceof PessoaJuridica) {
            return $this->cadastrarPessoaJuridica();
        } else if ($this->participante instanceof Treinador) {
            return $this->cadastrarTreinador();
        }
    }
    
    protected function cadastrarPessoaFisica()
    {
        return 'Cadastro de Pessoa Fisica realizado com sucesso!' . PHP_EOL;
    }
    
    protected function cadastrarPessoaJuridica()
    {
        return 'Cadastro de Pessoa Juridica realizado com sucesso!' . PHP_EOL;
    }
    
    protected function cadastrarTreinador()
    {
        return 'Cadastro de Treinador realizado com sucesso!' . PHP_EOL;
    }
}
