<?php 

require __DIR__ . '/vendor/autoload.php';

use FCL\Test2\Humano;
use FCL\Test2\Robo;

$humano = new Humano;
echo $humano->trabalhar();
echo $humano->descansar();
echo $humano->dormir();

echo "////////////////////////////////////////////////////////////////////////" . PHP_EOL;

$robo = new Robo;
echo $robo->trabalhar();
echo $robo->descansar();
echo $robo->dormir();

